#ifndef __MDNIE_H__
#define __MDNIE_H__

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

#define END_SEQ			0xffff

#ifdef CONFIG_FB_S5P_MDNIE_HIJACK
// Yank555.lu : Hijack switchzz
#define HIJACK_DISABLED	0
#define HIJACK_ENABLED	1
#endif

enum MODE {
	DYNAMIC,
	STANDARD,
#if !defined(CONFIG_FB_MDNIE_PWM)
	NATURAL,
#endif
	MOVIE,
#if !defined(CONFIG_CPU_EXYNOS4210)
	AUTO,
#endif
	MODE_MAX
};

enum SCENARIO {
#if defined(CONFIG_AOSP_ROM_SUPPORT)
	CYANOGENMOD_MODE,
#endif
	UI_MODE,
	VIDEO_MODE,
	VIDEO_WARM_MODE,
	VIDEO_COLD_MODE,
	CAMERA_MODE,
	NAVI_MODE,
	GALLERY_MODE,
	VT_MODE,
	SCENARIO_MAX,
};

#if defined(CONFIG_TARGET_LOCALE_KOR) || defined(CONFIG_TARGET_LOCALE_NTT)
enum SCENARIO_DMB {
	DMB_NORMAL_MODE = 20,
	DMB_WARM_MODE,
	DMB_COLD_MODE,
	DMB_MODE_MAX,
};
#endif

enum SCENARIO_COLOR_TONE {
	COLOR_TONE_1 = 40,
	COLOR_TONE_2,
	COLOR_TONE_3,
	COLOR_TONE_MAX,
};

enum OUTDOOR {
	OUTDOOR_OFF,
	OUTDOOR_ON,
	OUTDOOR_MAX,
};

enum TONE {
	TONE_NORMAL,
	TONE_WARM,
	TONE_COLD,
	TONE_MAX,
};

enum CABC {
	CABC_OFF,
#if defined(CONFIG_FB_MDNIE_PWM)
	CABC_ON,
#endif
	CABC_MAX,
};

enum POWER_LUT {
	LUT_DEFAULT,
	LUT_VIDEO,
	LUT_MAX,
};

enum POWER_LUT_LEVEL {
	LUT_LEVEL_MANUAL_AND_INDOOR,
	LUT_LEVEL_OUTDOOR_1,
	LUT_LEVEL_OUTDOOR_2,
	LUT_LEVEL_MAX,
};

enum NEGATIVE {
	NEGATIVE_OFF,
	NEGATIVE_ON,
	NEGATIVE_MAX,
};
enum ACCESSIBILITY {
	ACCESSIBILITY_OFF,
	NEGATIVE,
	COLOR_BLIND,
	ACCESSIBILITY_MAX
};
struct mdnie_tuning_info {
	const char *name;
	unsigned short *sequence;
};

struct mdnie_backlight_value {
	const unsigned int max;
	const unsigned int mid;
	const unsigned char low;
	const unsigned char dim;
};
struct mdnie_tuning_info_cabc {
	char *name;
	unsigned short *seq;
	unsigned int idx_lut;
};
struct mdnie_info {
	struct device			*dev;
#if defined(CONFIG_FB_MDNIE_PWM)
	struct lcd_platform_data	*lcd_pd;
	struct backlight_device		*bd;
	unsigned int			bd_enable;
	unsigned int			auto_brightness;
	unsigned int			power_lut_idx;
	struct mdnie_backlight_value	*backlight;
#endif
	struct mutex			lock;
	struct mutex			dev_lock;

	unsigned int enable;
	enum SCENARIO scenario;
	enum MODE mode;
	enum TONE tone;
	enum OUTDOOR outdoor;
	enum CABC cabc;
	unsigned int tuning;
	unsigned int accessibility;
	unsigned int color_correction;
	char path[50];
	unsigned int tunning;
	unsigned int negative;
#ifdef CONFIG_HAS_EARLYSUSPEND
	struct early_suspend		early_suspend;
#endif
};

extern struct mdnie_info *g_mdnie;

#if defined(CONFIG_FB_MDNIE_PWM)
extern void set_mdnie_pwm_value(struct mdnie_info *mdnie, int value);
#endif
extern int mdnie_calibration(unsigned short x, unsigned short y, int *r);
extern int mdnie_request_firmware(const char *path, u16 **buf, const char *name);
extern int mdnie_open_file(const char *path, char **fp);
extern int mdnie_txtbuf_to_parsing(char const *pFilepath);

extern void check_lcd_type(void);

#endif /* __MDNIE_H__ */
